// using System.Collections.Generic;
// using UnityEngine;

// public class Particle
// {
//     private Vector3 Acceleration { get; set; }
//     public Vector3 Velocity { get; set; }
//     public Vector3 Location { get; set; }
//     private float lifespan;
//     public GameObject gameObject;
//     private Renderer renderer;
//     private float initialSize;
//     private static readonly Color baseColor = new Color(0.5f, 0.5f, 0.5f);

//     public float Density { get; set; }
//     public float Pressure { get; set; }

//     public Particle(Vector3 l, GameObject particlePrefab)
//     {
//         Acceleration = Vector3.zero;
//         Velocity = new Vector3(0.5f, Random.Range(-0.05f, 0.05f), Random.Range(-0.05f, 0.05f));
//         Location = l;
//         gameObject = GameObject.Instantiate(particlePrefab, l, Quaternion.identity);
//         renderer = gameObject.GetComponent<Renderer>();
//         initialSize = gameObject.transform.localScale.x;
//         lifespan = 100.0f;
//         Density = 0f;
//         Pressure = 0f;
//     }

//     public void Reset(Vector3 l)
//     {
//         Acceleration = Vector3.zero;
//         Velocity = new Vector3(0.5f, Random.Range(-0.05f, 0.05f), Random.Range(-0.05f, 0.05f));
//         Location = l;
//         lifespan = 100.0f;
//         Density = 0f;
//         Pressure = 0f;
//         if (gameObject != null)
//         {
//             gameObject.transform.position = l;
//             gameObject.SetActive(true);
//         }
//     }

//     public void Run()
//     {
//         Update();
//     }

//     public void ApplyForce(Vector3 f)
//     {
//         Acceleration += f;
//     }

//     private void Update()
//     {
//         Velocity += Acceleration;
//         Location += Velocity * Time.deltaTime;

//         if (gameObject != null)
//         {
//             gameObject.transform.position = Location;
//             float sizeFactor = Mathf.Lerp(initialSize, 0, 1 - (lifespan / 100.0f));
//             gameObject.transform.localScale = new Vector3(sizeFactor, sizeFactor, sizeFactor);
//             renderer.material.color = new Color(baseColor.r, baseColor.g, baseColor.b, lifespan / 100.0f);
//         }

//         lifespan -= 1.0f;
//         Acceleration = Vector3.zero;
//         if (lifespan <= 0.0f && gameObject != null)
//         {
//             gameObject.SetActive(false);
//         }
//     }

//     public bool Dead()
//     {
//         return lifespan <= 0.0f;
//     }

//     public void DestroyGameObject()
//     {
//         if (gameObject != null)
//         {
//             GameObject.Destroy(gameObject);
//         }
//     }
// }


// public class ParticleSystemController3D : MonoBehaviour
// {
//     private List<Particle> particles;
//     private Stack<Particle> particlePool;
//     public GameObject particlePrefab;
//     private Vector3 origin;

//     public float kernelRadius = 1.0f;
//     public float particleMass = 0.1f;
//     public float restDensity = 1.0f;
//     public float stiffness = 1.0f;
//     public float viscosity = 0.1f;

//     private int gridResolution = 10;
//     private float cellSize;
//     private List<Particle>[,,] grid;

//     private void Start()
//     {
//         int numParticles = 100;
//         particles = new List<Particle>(numParticles);
//         particlePool = new Stack<Particle>(numParticles);
//         origin = new Vector3(0.0f, 0.0f, 0.0f);

//         cellSize = kernelRadius * 2;
//         grid = new List<Particle>[gridResolution, gridResolution, gridResolution];

//         for (int i = 0; i < gridResolution; i++)
//         {
//             for (int j = 0; j < gridResolution; j++)
//             {
//                 for (int k = 0; k < gridResolution; k++)
//                 {
//                     grid[i, j, k] = new List<Particle>();
//                 }
//             }
//         }

//         for (int i = 0; i < numParticles; i++)
//         {
//             Particle p = new Particle(origin, particlePrefab);
//             particles.Add(p);
//             particlePool.Push(p);
//         }
//     }

//     private void FixedUpdate()
//     {
//         Vector3 wind = new Vector3(0.1f, 0.0f, 0.0f);
//         ApplyForce(wind);
//         ComputeDensityAndPressure();
//         ComputeForces();
//         Run();
//         AddParticle();
//     }

//     private void ComputeDensityAndPressure()
//     {
//         ClearGrid();
//         PopulateGrid();

//         foreach (Particle p in particles)
//         {
//             float density = 0f;
//             List<Particle> neighbors = GetNeighbors(p);

//             foreach (Particle neighbor in neighbors)
//             {
//                 Vector3 diff = p.Location - neighbor.Location;
//                 float r2 = diff.sqrMagnitude;
//                 if (r2 < kernelRadius * kernelRadius)
//                 {
//                     density += particleMass * Mathf.Pow(kernelRadius * kernelRadius - r2, 3);
//                 }
//             }
//             p.Density = density;
//             p.Pressure = stiffness * (density - restDensity);
//         }
//     }

//     private void ComputeForces()
//     {
//         foreach (Particle p in particles)
//         {
//             Vector3 pressureForce = Vector3.zero;
//             Vector3 viscosityForce = Vector3.zero;
//             List<Particle> neighbors = GetNeighbors(p);

//             foreach (Particle neighbor in neighbors)
//             {
//                 if (p == neighbor) continue;

//                 Vector3 diff = p.Location - neighbor.Location;
//                 float r = diff.magnitude;
//                 if (r < kernelRadius && r > 0)
//                 {
//                     pressureForce -= diff.normalized * particleMass *
//                                      (p.Pressure + neighbor.Pressure) / (2 * neighbor.Density) *
//                                      Mathf.Pow(kernelRadius - r, 2);

//                     viscosityForce += viscosity * particleMass *
//                                       (neighbor.Velocity - p.Velocity) / neighbor.Density *
//                                       (kernelRadius - r);
//                 }
//             }
//             p.ApplyForce(pressureForce);
//             p.ApplyForce(viscosityForce);
//         }
//     }

//     private void Run()
//     {
//         for (int i = particles.Count - 1; i >= 0; i--)
//         {
//             Particle particle = particles[i];
//             if (particle.Dead())
//             {
//                 particles.RemoveAt(i);
//                 particlePool.Push(particle);
//             }
//             else
//             {
//                 particle.Run();
//             }
//         }
//     }

//     private void ApplyForce(Vector3 dir)
//     {
//         foreach (Particle p in particles)
//         {
//             p.ApplyForce(dir);
//         }
//     }

//     private void AddParticle()
//     {
//         for (int i = 0; i < 3; i++)
//         {
//             if (particlePool.Count > 0)
//             {
//                 Particle p = particlePool.Pop();
//                 p.Reset(origin);
//                 particles.Add(p);
//             }
//             else
//             {
//                 Particle p = new Particle(origin, particlePrefab);
//                 particles.Add(p);
//             }
//         }
//     }

//     private void ClearGrid()
//     {
//         foreach (var cell in grid)
//         {
//             cell.Clear();
//         }
//     }

//     private void PopulateGrid()
//     {
//         foreach (Particle p in particles)
//         {
//             Vector3 pos = p.Location + new Vector3(5f, 5f, 5f); // shift to positive coordinates
//             int x = Mathf.Clamp((int)(pos.x / cellSize), 0, gridResolution - 1);
//             int y = Mathf.Clamp((int)(pos.y / cellSize), 0, gridResolution - 1);
//             int z = Mathf.Clamp((int)(pos.z / cellSize), 0, gridResolution - 1);
//             grid[x, y, z].Add(p);
//         }
//     }

//     private List<Particle> GetNeighbors(Particle p)
//     {
//         List<Particle> neighbors = new List<Particle>();
//         Vector3 pos = p.Location + new Vector3(5f, 5f, 5f); // shift to positive coordinates
//         int x = Mathf.Clamp((int)(pos.x / cellSize), 0, gridResolution - 1);
//         int y = Mathf.Clamp((int)(pos.y / cellSize), 0, gridResolution - 1);
//         int z = Mathf.Clamp((int)(pos.z / cellSize), 0, gridResolution - 1);

//         for (int i = Mathf.Max(0, x - 1); i <= Mathf.Min(gridResolution - 1, x + 1); i++)
//         {
//             for (int j = Mathf.Max(0, y - 1); j <= Mathf.Min(gridResolution - 1, y + 1); j++)
//             {
//                 for (int k = Mathf.Max(0, z - 1); k <= Mathf.Min(gridResolution - 1, z + 1); k++)
//                 {
//                     neighbors.AddRange(grid[i, j, k]);
//                 }
//             }
//         }
//         return neighbors;
//     }
// }

using System.Collections.Generic;
using UnityEngine;

public class Particle
{
    private Vector3 Acceleration { get; set; }
    public Vector3 Velocity { get; set; }
    public Vector3 Location { get; set; }
    private float lifespan;
    public GameObject gameObject;
    private Renderer renderer;
    private float initialSize;
    private static readonly Color baseColor = new Color(0.5f, 0.5f, 0.5f);

    public float Density { get; set; }
    public float Pressure { get; set; }

    public Particle(Vector3 l, GameObject particlePrefab)
    {
        Acceleration = Vector3.zero;
        Velocity = new Vector3(0.5f, Random.Range(-0.05f, 0.05f), Random.Range(-0.05f, 0.05f));
        Location = l;
        gameObject = GameObject.Instantiate(particlePrefab, l, Quaternion.identity);
        renderer = gameObject.GetComponent<Renderer>();
        initialSize = gameObject.transform.localScale.x;
        lifespan = 100.0f;
        Density = 0f;
        Pressure = 0f;
    }
    public void Reset(Vector3 l)
    {
        Acceleration = Vector3.zero;
        Velocity = new Vector3(0.5f, Random.Range(-0.05f, 0.05f), Random.Range(-0.05f, 0.05f));
        Location = l;
        lifespan = 100.0f;
        Density = 0f;
        Pressure = 0f;
        if (gameObject != null)
        {
            gameObject.transform.position = l;
            gameObject.SetActive(true);
        }
    }

    public void Run()
    {
        Update();
    }

    public void ApplyForce(Vector3 f)
    {
        Acceleration += f;
    }

    private void Update()
    {
        Velocity += Acceleration;
        Location += Velocity * Time.deltaTime;

        if (gameObject != null)
        {
            gameObject.transform.position = Location;
            float sizeFactor = Mathf.Lerp(initialSize, 0, 1 - (lifespan / 100.0f));
            gameObject.transform.localScale = new Vector3(sizeFactor, sizeFactor, sizeFactor);
            renderer.material.color = new Color(baseColor.r, baseColor.g, baseColor.b, lifespan / 100.0f);
        }

        lifespan -= 1.0f;
        Acceleration = Vector3.zero;
        if (lifespan <= 0.0f && gameObject != null)
        {
            gameObject.SetActive(false);
        }
    }

    public bool Dead()
    {
        return lifespan <= 0.0f;
    }

    public void DestroyGameObject()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }
}

public class ParticleSystemController3D : MonoBehaviour
{
    private List<Particle> particles;
    private Stack<Particle> particlePool;
    public GameObject particlePrefab;
    private Vector3 origin;

    public float kernelRadius = 1.0f;
    public float particleMass = 0.1f;
    public float restDensity = 1.0f;
    public float stiffness = 1.0f;
    public float viscosity = 0.1f;

    private int gridResolution = 10;
    private float cellSize;
    private List<Particle>[,,] grid;

    private void Start()
    {
        int numParticles = 100;
        particles = new List<Particle>(numParticles);
        particlePool = new Stack<Particle>(numParticles);
        origin = new Vector3(0.0f, 0.0f, 0.0f);

        cellSize = kernelRadius * 2;
        grid = new List<Particle>[gridResolution, gridResolution, gridResolution];

        for (int i = 0; i < gridResolution; i++)
        {
            for (int j = 0; j < gridResolution; j++)
            {
                for (int k = 0; k < gridResolution; k++)
                {
                    grid[i, j, k] = new List<Particle>();
                }
            }
        }

        for (int i = 0; i < numParticles; i++)
        {
            Particle p = new Particle(origin, particlePrefab);
            particles.Add(p);
            particlePool.Push(p);
        }
    }

    private void FixedUpdate()
    {
        Vector3 wind = new Vector3(0.1f, 0.0f, 0.0f);
        ApplyForce(wind);
        ComputeDensityAndPressure();
        ComputeForces();
        Run();
        AddParticle();
    }

    private void ComputeDensityAndPressure()
    {
        ClearGrid();
        PopulateGrid();

        foreach (Particle p in particles)
        {
            float density = 0f;
            List<Particle> neighbors = GetNeighbors(p);

            foreach (Particle neighbor in neighbors)
            {
                Vector3 diff = p.Location - neighbor.Location;
                float r2 = diff.sqrMagnitude;
                if (r2 < kernelRadius * kernelRadius)
                {
                    density += particleMass * Mathf.Pow(kernelRadius * kernelRadius - r2, 3);
                }
            }
            p.Density = density;
            p.Pressure = stiffness * (density - restDensity);
        }
    }

    private void ComputeForces()
    {
        foreach (Particle p in particles)
        {
            Vector3 pressureForce = Vector3.zero;
            Vector3 viscosityForce = Vector3.zero;
            List<Particle> neighbors = GetNeighbors(p);

            foreach (Particle neighbor in neighbors)
            {
                if (p == neighbor) continue;

                Vector3 diff = p.Location - neighbor.Location;
                float r = diff.magnitude;
                if (r < kernelRadius && r > 0)
                {
                    pressureForce -= diff.normalized * particleMass *
                                     (p.Pressure + neighbor.Pressure) / (2 * neighbor.Density) *
                                     Mathf.Pow(kernelRadius - r, 2);

                    viscosityForce += viscosity * particleMass *
                                      (neighbor.Velocity - p.Velocity) / neighbor.Density *
                                      (kernelRadius - r);
                }
            }
            p.ApplyForce(pressureForce);
            p.ApplyForce(viscosityForce);
        }
    }

    private void Run()
    {
        for (int i = particles.Count - 1; i >= 0; i--)
        {
            Particle particle = particles[i];
            if (particle.Dead())
            {
                particles.RemoveAt(i);
                particlePool.Push(particle);
            }
            else
            {
                particle.Run();
            }
        }
    }

    private void ApplyForce(Vector3 dir)
    {
        foreach (Particle p in particles)
        {
            p.ApplyForce(dir);
        }
    }

    private void AddParticle()
    {
        for (int i = 0; i < 3; i++)
        {
            if (particlePool.Count > 0)
            {
                Particle p = particlePool.Pop();
                p.Reset(origin);
                particles.Add(p);
            }
            else
            {
                Particle p = new Particle(origin, particlePrefab);
                particles.Add(p);
            }
        }
    }

    private void ClearGrid()
    {
        foreach (var cell in grid)
        {
            cell.Clear();
        }
    }

    private void PopulateGrid()
    {
        foreach (Particle p in particles)
        {
            Vector3 pos = p.Location + new Vector3(5f, 5f, 5f); // shift to positive coordinates
            int x = Mathf.Clamp((int)(pos.x / cellSize), 0, gridResolution - 1);
            int y = Mathf.Clamp((int)(pos.y / cellSize), 0, gridResolution - 1);
            int z = Mathf.Clamp((int)(pos.z / cellSize), 0, gridResolution - 1);
            grid[x, y, z].Add(p);
        }
    }

    private List<Particle> GetNeighbors(Particle p)
    {
        List<Particle> neighbors = new List<Particle>();
        Vector3 pos = p.Location + new Vector3(5f, 5f, 5f); // shift to positive coordinates
        int x = Mathf.Clamp((int)(pos.x / cellSize), 0, gridResolution - 1);
        int y = Mathf.Clamp((int)(pos.y / cellSize), 0, gridResolution - 1);
        int z = Mathf.Clamp((int)(pos.z / cellSize), 0, gridResolution - 1);

        for (int i = Mathf.Max(0, x - 1); i <= Mathf.Min(gridResolution - 1, x + 1); i++)
        {
            for (int j = Mathf.Max(0, y - 1); j <= Mathf.Min(gridResolution - 1, y + 1); j++)
            {
                for (int k = Mathf.Max(0, z - 1); k <= Mathf.Min(gridResolution - 1, z + 1); k++)
                {
                    neighbors.AddRange(grid[i, j, k]);
                }
            }
        }
        return neighbors;
    }
}
